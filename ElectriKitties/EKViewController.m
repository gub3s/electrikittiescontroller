//
//  EKViewController.m
//  ElectriKitties
//
//  Created by Samuel Gubler on 1/16/14.
//  Copyright (c) 2014 Samuel Gubler. All rights reserved.
//

#import "EKViewController.h"

@interface EKViewController ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *outputLabel;

@property (nonatomic, strong) NSString *angleOutputString;
@property (nonatomic, strong) NSString *forceOutputString;
@property (nonatomic, strong) NSString *chargeOutputString;

@end

@implementation EKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.angleOutputString = @"Current Angle: ";
    self.forceOutputString = @"Current Force: ";
    self.chargeOutputString = @"Current Charge: ";
    [self segmentedControlValueChanged:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segmentedControlValueChanged:(id)sender
{
    if ([self.segmentedControl selectedSegmentIndex] == 0) {
        self.outputLabel.text = self.angleOutputString;
    } else if ([self.segmentedControl selectedSegmentIndex] == 1) {
        self.outputLabel.text = self.forceOutputString;
    } else {
        self.outputLabel.text = self.chargeOutputString;
    }
}


@end
