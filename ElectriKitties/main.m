//
//  main.m
//  ElectriKitties
//
//  Created by Samuel Gubler on 1/16/14.
//  Copyright (c) 2014 Samuel Gubler. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EKAppDelegate class]));
    }
}
